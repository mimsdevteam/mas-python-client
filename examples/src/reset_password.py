import mas
import helpers
import sys

# validate parameters
if len(sys.argv) < 5:
    print("Not enough parameters. | mas-url username old-password new-password")
    sys.exit()

# initialize MAS client
masUrl = sys.argv[1]
masConfig = mas.Configuration()
masConfig.host = masUrl
masClient = mas.ApiClient(masConfig)

# login
username = sys.argv[2]
oldPassword = sys.argv[3]
newPassword = sys.argv[4]
accessToken = helpers.auth.login(masClient, username, oldPassword)

# reset password
helpers.auth.reset_password(masClient, accessToken, oldPassword, newPassword)

print("Done")