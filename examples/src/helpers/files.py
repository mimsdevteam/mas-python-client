import mas
import math
import os
import sys
import requests
import json

def upload_file(masClient, accessToken, path, parentId="root"):

    files = mas.FilesApi(masClient)
    with open(path, 'rb') as f:
        size = os.path.getsize(path)
        body = {
            "is_folder": False,
            "size": size,
            "title": os.path.basename(path),
        }
        uploadId = files.post_file(parentId, accessToken, body).id
        #todo: handle error here ^
    
        maxChunkSize = 100000000
        offset = 0

        result_id = ""

        # upload chunk by chunk
        while True:
            data = f.read(maxChunkSize)
            len_data = len(data)
            if not len_data:
                break
            limit = min(size, offset + len_data)
            
            resp = requests.patch(masClient.configuration.host + f'/mas/fs/upload/{uploadId}', headers={
                'X-Access-Token': accessToken,
                'Content-Range': "bytes " + str(offset) + "-" + str(limit) + "/" + str(size),
                'Content-Length': str(len_data),
                'Content-Type': 'application/octet-stream'
            }, data=data)

            r = resp.json()
            result_id = r['id']
            offset += len_data
            #todo: handle error here ^

        return result_id

def update_metadata(masClient, accessToken, fileId, metadata):
    files = mas.FilesApi(masClient)
    body = {
        "metadata": metadata
    }
    return files.file_update(accessToken, fileId, body)
    #todo: handle error here ^
        