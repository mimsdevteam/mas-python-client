import mas
import pyotp
import time

def login (masClient, username, password):
    auth = mas.AuthenticationApi(masClient)

    # Get Google Authenticator secret
    googleAuthFile = open("keys/google-auth", "r")
    url = googleAuthFile.read()
    googleAuthFile.close()

    if url == "":
        print("Can't find valid Google Auth file")
        exit()

    # Pass first login step
    firstFactorToken = auth.first_factor_login(mas.ModelsLoginRequest(username, password)).first_factor_token

    # Google Authenticator Code
    code = pyotp.parse_uri(url).now()
    request = mas.ModelsValidateSecondFactorRequest(code)

    return auth.validate_second_factor(firstFactorToken, request).access_token

def reset_password(masClient, accessToken, oldPassword, newPassword):
    auth = mas.AuthenticationApi(masClient)
    body = mas.ModelsOwnPasswordResetRequest(old_password=oldPassword, new_password=newPassword)
    auth.reset_own_password(accessToken, body)