import mas
import helpers
import sys

# validate parameters
if len(sys.argv) < 5:
    print("Not enough parameters. | mas-url username password file-path")
    sys.exit()

# initialize MAS client
masUrl = sys.argv[1]
masConfig = mas.Configuration()
masConfig.host = masUrl
masClient = mas.ApiClient(masConfig)

# login
username = sys.argv[2]
password = sys.argv[3]
accessToken = helpers.auth.login(masClient, username, password)

# upload file
print("Uploading...")
filePath = sys.argv[4]
fileId = helpers.files.upload_file(masClient, accessToken, filePath)
print("Uploaded File ID: " + fileId)
print("Done")