import mas
import helpers
import sys

# validate parameters
if len(sys.argv) < 4:
    print("Not enough parameters. | mas-url username password")
    sys.exit()

# Initialize Mas Client
configuration = mas.Configuration()
configuration.host = sys.argv[1]
masClient = mas.ApiClient(configuration)

# Login with user/password + second factor
username = sys.argv[2]
password = sys.argv[3]

accessToken = helpers.auth.login(masClient, username, password)

print(accessToken)