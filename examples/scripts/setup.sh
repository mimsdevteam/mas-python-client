PYTHON_INSTALLED=0
command -v python3 >/dev/null 2>&1 && PYTHON_INSTALLED=1
if [ $PYTHON_INSTALLED == 0 ];
then
    echo "Error: Python 3 needs to be installed in order to setup and run this project."
    exit
fi

echo "Installing dependencies..."
pip install pyotp

pip install "git+https://bitbucket.org/mimsdevteam/mas-python-client.git/"

echo "Done"

