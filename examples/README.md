# mas-python-client examples

(c) Copyright My Intelligent Machines

Works on Linux (Bash + Python)

## How to setup

---

1. Clone this repository and go to `mas-python-client/examples`

2. Run `scripts/setup.sh`

3. Replace `keys/google-auth` with the one that was provided to you (for 2FA)

## How to run

---

### Parameters

- _[mas-url]_ : MAS server address
- _[username]_ : username for login
- _[password]_ : password for login
- _[file-path]_ : path of the file to upload

### Commands


**Upload a file**

`scripts/upload.sh [mas-url] [username] [password] [file-path]`

1. Login to the platform

2. Upload a big file chunk by chunk

3. Update that file's metadata

4. Print out "Done"



**Upload a folder**

`scripts/upload_folder.sh [mas-url] [username] [password] [file-path]`

1. Login to the platform

2. Upload a list files chunk by chunk

3. Print out "Done"


**Login**

To test your access.

`scripts/login.sh [mas-url] [username] [password]`

1. Pass the first login step with username and password

2. Pass the second login step with Google Authenticator's second factor

3. Print out the access token

Note that on the first successful attempt, the authenticator's secret is saved in `./keys`