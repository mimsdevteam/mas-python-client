# mas
(c) Copyright My Intelligent Machines

- API version: v1.7.3
- Package version: 1.0.0
- Build package: io.swagger.codegen.languages.PythonClientCodegen
For more information, please visit [https://mims.ai](https://mims.ai)

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import mas 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import mas
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import mas
from mas.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = mas.AuthenticationApi(mas.ApiClient(configuration))
x_access_token = 'x_access_token_example' # str | Access Token
body = mas.ModelsActivationRequest() # ModelsActivationRequest | 

try:
    # Activates an account
    api_instance.activate(x_access_token, body)
except ApiException as e:
    print("Exception when calling AuthenticationApi->activate: %s\n" % e)

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost*

| Class               | Method                                                                               | HTTP request                                                   | Description                                                                                |
| ------------------- | ------------------------------------------------------------------------------------ | -------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| *AuthenticationApi* | [**activate**](docs/AuthenticationApi.md#activate)                                   | **POST** /mas/auth/activate                                    | Activates an account                                                                       |
| *AuthenticationApi* | [**activate_account**](docs/AuthenticationApi.md#activate_account)                   | **GET** /mas/auth/activate/{token}                             | Returns success and activates the account if token is a match                              |
| *AuthenticationApi* | [**deactivate**](docs/AuthenticationApi.md#deactivate)                               | **POST** /mas/auth/deactivate                                  | Deactivates an account                                                                     |
| *AuthenticationApi* | [**first_factor_login**](docs/AuthenticationApi.md#first_factor_login)               | **POST** /mas/auth/login/v2                                    | Returns access and refresh tokens                                                          |
| *AuthenticationApi* | [**get_google_auth_url**](docs/AuthenticationApi.md#get_google_auth_url)             | **GET** /mas/auth/get_google_auth_url                          | Returns an URL to be used with Google Authenticator                                        |
| *AuthenticationApi* | [**get_google_qr_code**](docs/AuthenticationApi.md#get_google_qr_code)               | **GET** /mas/auth/get_google_qrcode                            | Returns a PNG to scan for user waiting for second factor                                   |
| *AuthenticationApi* | [**internal_login**](docs/AuthenticationApi.md#internal_login)                       | **POST** /internal/auth/login                                  | Returns access and refresh tokens for internal services                                    |
| *AuthenticationApi* | [**internalize_access**](docs/AuthenticationApi.md#internalize_access)               | **POST** /internal/auth/internalize-access                     | Takes a normal access token and returns a new internal one                                 |
| *AuthenticationApi* | [**is_authorized**](docs/AuthenticationApi.md#is_authorized)                         | **POST** /mas/auth/is_authorized                               | Returns true/false for a set of permissions, for current user                              |
| *AuthenticationApi* | [**login**](docs/AuthenticationApi.md#login)                                         | **POST** /mas/auth/login                                       | Returns a JWT token when successful                                                        |
| *AuthenticationApi* | [**logout**](docs/AuthenticationApi.md#logout)                                       | **POST** /mas/auth/logout                                      | Blacklist refresh token                                                                    |
| *AuthenticationApi* | [**refresh_access**](docs/AuthenticationApi.md#refresh_access)                       | **POST** /mas/auth/refresh                                     | Refresh accessToken with refreshToken                                                      |
| *AuthenticationApi* | [**reset_own_password**](docs/AuthenticationApi.md#reset_own_password)               | **POST** /mas/auth/reset-own-password                          | Resets a user&#39;s own password                                                           |
| *AuthenticationApi* | [**reset_password**](docs/AuthenticationApi.md#reset_password)                       | **POST** /mas/auth/reset-password                              | Resets a password                                                                          |
| *AuthenticationApi* | [**reset_password_with_token**](docs/AuthenticationApi.md#reset_password_with_token) | **POST** /internal/auth/reset-password-with-token              | Method for reseting a password, needs a token sent by mail                                 |
| *AuthenticationApi* | [**send_password_reset_email**](docs/AuthenticationApi.md#send_password_reset_email) | **POST** /internal/auth/send-password-reset-email              | Send an email for resetting password                                                       |
| *AuthenticationApi* | [**validate_second_factor**](docs/AuthenticationApi.md#validate_second_factor)       | **POST** /mas/auth/validate-second-factor                      | Validate a second factor token                                                             |
| *AuthenticationApi* | [**verify_token**](docs/AuthenticationApi.md#verify_token)                           | **GET** /mas/auth/verify                                       | Verify JWT token                                                                           |
| *FilesApi*          | [**compress**](docs/FilesApi.md#compress)                                            | **POST** /mas/fs/files/compress                                | Compress files into an archive                                                             |
| *FilesApi*          | [**delete_file**](docs/FilesApi.md#delete_file)                                      | **DELETE** /mas/fs/files/{file_id}                             | Deletes a file resource                                                                    |
| *FilesApi*          | [**download_file**](docs/FilesApi.md#download_file)                                  | **GET** /mas/fs/files/{file_id}/data                           | Download file&#39;s data or stats                                                          |
| *FilesApi*          | [**file_move**](docs/FilesApi.md#file_move)                                          | **PATCH** /mas/fs/files/{file_id}/move                         | Move a file resource                                                                       |
| *FilesApi*          | [**file_perm_delete**](docs/FilesApi.md#file_perm_delete)                            | **DELETE** /mas/fs/files/{file_id}/permissions/{perm_id}       | Remove a permission from a file object (unshare a file)                                    |
| *FilesApi*          | [**file_perm_update**](docs/FilesApi.md#file_perm_update)                            | **PATCH** /mas/fs/files/{file_id}/permissions/{perm_id}        | Update a permission in a file resource                                                     |
| *FilesApi*          | [**file_restore**](docs/FilesApi.md#file_restore)                                    | **POST** /mas/fs/files/{file_id}/restore                       | Restore a file resource                                                                    |
| *FilesApi*          | [**file_update**](docs/FilesApi.md#file_update)                                      | **PATCH** /mas/fs/files/{file_id}                              | Update a file resource                                                                     |
| *FilesApi*          | [**from_path**](docs/FilesApi.md#from_path)                                          | **GET** /mas/fs/internal/filefrompath                          | Retrieve a file resource from its path                                                     |
| *FilesApi*          | [**get_child_id**](docs/FilesApi.md#get_child_id)                                    | **GET** /mas/fs/internal/get-child-id/{parent_id}/{child_name} | Returns a fileID if a child with same name than {child_name} exists for parent {parent_id} |
| *FilesApi*          | [**get_file**](docs/FilesApi.md#get_file)                                            | **GET** /mas/fs/files/{file_id}                                | Retrieve a full description and download URL of a file resource                            |
| *FilesApi*          | [**get_file_path_from_id**](docs/FilesApi.md#get_file_path_from_id)                  | **GET** /mas/fs/files/{file_id}/path                           | Get file/folder full path from its ID                                                      |
| *FilesApi*          | [**list_files**](docs/FilesApi.md#list_files)                                        | **GET** /mas/fs/files                                          | List file resources                                                                        |
| *FilesApi*          | [**list_folder**](docs/FilesApi.md#list_folder)                                      | **GET** /mas/fs/files/{folder_id}/ls                           | List a folder&#39;s content                                                                |
| *FilesApi*          | [**post_file**](docs/FilesApi.md#post_file)                                          | **POST** /mas/fs/files/{file_id}                               | Create a folder, or start a data upload session.                                           |
| *FilesApi*          | [**post_file_permission**](docs/FilesApi.md#post_file_permission)                    | **POST** /mas/fs/files/{file_id}/permissions                   | Insert a new permission in a file resource (share a file)                                  |
| *FilesApi*          | [**remaining_bytes**](docs/FilesApi.md#remaining_bytes)                              | **HEAD** /mas/fs/upload/{upload_session_id}                    | Sends back how much bytes were sent                                                        |
| *FilesApi*          | [**upload**](docs/FilesApi.md#upload)                                                | **PATCH** /mas/fs/upload/{upload_session_id}                   | Upload a Chunk of a file or a file if it&#39;s size is less than 100mib                    |
| *FilesApi*          | [**upload_metadata**](docs/FilesApi.md#upload_metadata)                              | **PATCH** /mas/fs/files/{file_id}/metadata                     | Replace file&#39;s metadata by uploading a CSV/JSON metadata file                          |


## Documentation For Models

 - [ApisAPIError](docs/ApisAPIError.md)
 - [ApisAPIErrorData](docs/ApisAPIErrorData.md)
 - [ModelsActivationRequest](docs/ModelsActivationRequest.md)
 - [ModelsCompressFileData](docs/ModelsCompressFileData.md)
 - [ModelsDashboardFileUploadRequest](docs/ModelsDashboardFileUploadRequest.md)
 - [ModelsDeactivationRequest](docs/ModelsDeactivationRequest.md)
 - [ModelsEmail](docs/ModelsEmail.md)
 - [ModelsEntity](docs/ModelsEntity.md)
 - [ModelsFile](docs/ModelsFile.md)
 - [ModelsFileCapabilities](docs/ModelsFileCapabilities.md)
 - [ModelsFileGroup](docs/ModelsFileGroup.md)
 - [ModelsFileGroupInfo](docs/ModelsFileGroupInfo.md)
 - [ModelsFileGroupShort](docs/ModelsFileGroupShort.md)
 - [ModelsFileGroupShortList](docs/ModelsFileGroupShortList.md)
 - [ModelsFileGroupUpdateData](docs/ModelsFileGroupUpdateData.md)
 - [ModelsFilePathPartID](docs/ModelsFilePathPartID.md)
 - [ModelsFilePathResponse](docs/ModelsFilePathResponse.md)
 - [ModelsFileShort](docs/ModelsFileShort.md)
 - [ModelsFileShortList](docs/ModelsFileShortList.md)
 - [ModelsFirstFactorLoginResponse](docs/ModelsFirstFactorLoginResponse.md)
 - [ModelsGetChildIDResponse](docs/ModelsGetChildIDResponse.md)
 - [ModelsGetGoogleAuthURLResponse](docs/ModelsGetGoogleAuthURLResponse.md)
 - [ModelsGetUserIDResponse](docs/ModelsGetUserIDResponse.md)
 - [ModelsGroupCapabilities](docs/ModelsGroupCapabilities.md)
 - [ModelsInputMatchFailReason](docs/ModelsInputMatchFailReason.md)
 - [ModelsInstanceParameter](docs/ModelsInstanceParameter.md)
 - [ModelsInternalizeAccessRequest](docs/ModelsInternalizeAccessRequest.md)
 - [ModelsInternalizeAccessResponse](docs/ModelsInternalizeAccessResponse.md)
 - [ModelsIsAuthorizedRequest](docs/ModelsIsAuthorizedRequest.md)
 - [ModelsIsAuthorizedRequestPermissions](docs/ModelsIsAuthorizedRequestPermissions.md)
 - [ModelsIsAuthorizedResponse](docs/ModelsIsAuthorizedResponse.md)
 - [ModelsLegacyLoginResponse](docs/ModelsLegacyLoginResponse.md)
 - [ModelsLicense](docs/ModelsLicense.md)
 - [ModelsLicensePermissions](docs/ModelsLicensePermissions.md)
 - [ModelsLoginRequest](docs/ModelsLoginRequest.md)
 - [ModelsLoginResponse](docs/ModelsLoginResponse.md)
 - [ModelsLogoutRequest](docs/ModelsLogoutRequest.md)
 - [ModelsMetadata](docs/ModelsMetadata.md)
 - [ModelsMissingInput](docs/ModelsMissingInput.md)
 - [ModelsNameExistsResponse](docs/ModelsNameExistsResponse.md)
 - [ModelsNewID](docs/ModelsNewID.md)
 - [ModelsNotification](docs/ModelsNotification.md)
 - [ModelsNotificationList](docs/ModelsNotificationList.md)
 - [ModelsOwnPasswordResetRequest](docs/ModelsOwnPasswordResetRequest.md)
 - [ModelsPasswordResetEmailRequest](docs/ModelsPasswordResetEmailRequest.md)
 - [ModelsPasswordResetRequest](docs/ModelsPasswordResetRequest.md)
 - [ModelsPasswordResetWithTokenRequest](docs/ModelsPasswordResetWithTokenRequest.md)
 - [ModelsPasswordResetWithTokenResponse](docs/ModelsPasswordResetWithTokenResponse.md)
 - [ModelsPermission](docs/ModelsPermission.md)
 - [ModelsPermissionCreateObject](docs/ModelsPermissionCreateObject.md)
 - [ModelsPermissionCreateResponse](docs/ModelsPermissionCreateResponse.md)
 - [ModelsPermissionPatchObject](docs/ModelsPermissionPatchObject.md)
 - [ModelsPosition](docs/ModelsPosition.md)
 - [ModelsPostGroup](docs/ModelsPostGroup.md)
 - [ModelsPostObject](docs/ModelsPostObject.md)
 - [ModelsProject](docs/ModelsProject.md)
 - [ModelsProjectCapabilities](docs/ModelsProjectCapabilities.md)
 - [ModelsProjectCreateData](docs/ModelsProjectCreateData.md)
 - [ModelsProjectExecuteData](docs/ModelsProjectExecuteData.md)
 - [ModelsProjectInput](docs/ModelsProjectInput.md)
 - [ModelsProjectInputAddData](docs/ModelsProjectInputAddData.md)
 - [ModelsProjectShort](docs/ModelsProjectShort.md)
 - [ModelsProjectShortList](docs/ModelsProjectShortList.md)
 - [ModelsProjectUpdateData](docs/ModelsProjectUpdateData.md)
 - [ModelsProjectWorkflowInstance](docs/ModelsProjectWorkflowInstance.md)
 - [ModelsProjectWorkflowMatch](docs/ModelsProjectWorkflowMatch.md)
 - [ModelsRefreshRequest](docs/ModelsRefreshRequest.md)
 - [ModelsRefreshResponse](docs/ModelsRefreshResponse.md)
 - [ModelsRepositoryMethodTags](docs/ModelsRepositoryMethodTags.md)
 - [ModelsRepositoryMethods](docs/ModelsRepositoryMethods.md)
 - [ModelsRole](docs/ModelsRole.md)
 - [ModelsRolePermissions](docs/ModelsRolePermissions.md)
 - [ModelsSubscriptionLicenseData](docs/ModelsSubscriptionLicenseData.md)
 - [ModelsSubscriptionLicenseDataPermissions](docs/ModelsSubscriptionLicenseDataPermissions.md)
 - [ModelsTeam](docs/ModelsTeam.md)
 - [ModelsTeamAndUserEmails](docs/ModelsTeamAndUserEmails.md)
 - [ModelsTeamSubscriptionData](docs/ModelsTeamSubscriptionData.md)
 - [ModelsTokenData](docs/ModelsTokenData.md)
 - [ModelsUnfitUserInput](docs/ModelsUnfitUserInput.md)
 - [ModelsUpdateInfo](docs/ModelsUpdateInfo.md)
 - [ModelsUser](docs/ModelsUser.md)
 - [ModelsUserActivationToken](docs/ModelsUserActivationToken.md)
 - [ModelsUserCreateData](docs/ModelsUserCreateData.md)
 - [ModelsUserEmailExistence](docs/ModelsUserEmailExistence.md)
 - [ModelsUserGroup](docs/ModelsUserGroup.md)
 - [ModelsUserGroupCapabilities](docs/ModelsUserGroupCapabilities.md)
 - [ModelsUserGroupCreateData](docs/ModelsUserGroupCreateData.md)
 - [ModelsUserGroupList](docs/ModelsUserGroupList.md)
 - [ModelsUserGroupUpdateData](docs/ModelsUserGroupUpdateData.md)
 - [ModelsUserGroupUserInsertData](docs/ModelsUserGroupUserInsertData.md)
 - [ModelsUserGroupUserRoleUpdateData](docs/ModelsUserGroupUserRoleUpdateData.md)
 - [ModelsUserList](docs/ModelsUserList.md)
 - [ModelsUserListItem](docs/ModelsUserListItem.md)
 - [ModelsUserUpdateData](docs/ModelsUserUpdateData.md)
 - [ModelsValidateSecondFactorRequest](docs/ModelsValidateSecondFactorRequest.md)
 - [ModelsValidateSecondFactorResponse](docs/ModelsValidateSecondFactorResponse.md)
 - [ModelsWorkflow](docs/ModelsWorkflow.md)
 - [WeseventsRetryMessage](docs/WeseventsRetryMessage.md)
 - [WesmodelsChildren](docs/WesmodelsChildren.md)
 - [WesmodelsDefault](docs/WesmodelsDefault.md)
 - [WesmodelsExecResources](docs/WesmodelsExecResources.md)
 - [WesmodelsGraphConfiguration](docs/WesmodelsGraphConfiguration.md)
 - [WesmodelsGraphConfigurationNodeTemplateReferences](docs/WesmodelsGraphConfigurationNodeTemplateReferences.md)
 - [WesmodelsMethod](docs/WesmodelsMethod.md)
 - [WesmodelsMethodContext](docs/WesmodelsMethodContext.md)
 - [WesmodelsMethodInput](docs/WesmodelsMethodInput.md)
 - [WesmodelsMethodOutput](docs/WesmodelsMethodOutput.md)
 - [WesmodelsWorkflowInput](docs/WesmodelsWorkflowInput.md)
 - [WesmodelsWorkflowNode](docs/WesmodelsWorkflowNode.md)
 - [WesmodelsWorkflowNodeInput](docs/WesmodelsWorkflowNodeInput.md)
 - [WesmodelsWorkflowOutput](docs/WesmodelsWorkflowOutput.md)


## Documentation For Authorization

 All endpoints do not require authorization.