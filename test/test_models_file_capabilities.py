# coding: utf-8

"""
    MAS

    Mims Api Server  # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.models.models_file_capabilities import ModelsFileCapabilities  # noqa: E501
from mas.rest import ApiException


class TestModelsFileCapabilities(unittest.TestCase):
    """ModelsFileCapabilities unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testModelsFileCapabilities(self):
        """Test ModelsFileCapabilities"""
        # FIXME: construct object with mandatory attributes with example values
        # model = mas.models.models_file_capabilities.ModelsFileCapabilities()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
