# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.api.teams_api import TeamsApi  # noqa: E501
from mas.rest import ApiException


class TestTeamsApi(unittest.TestCase):
    """TeamsApi unit test stubs"""

    def setUp(self):
        self.api = mas.api.teams_api.TeamsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_license(self):
        """Test case for create_license

        Create a new license resource, need admin role  # noqa: E501
        """
        pass

    def test_create_subscription(self):
        """Test case for create_subscription

        Create a new subscription resource, need admin role  # noqa: E501
        """
        pass

    def test_create_team(self):
        """Test case for create_team

        Create a new team resource, need admin role  # noqa: E501
        """
        pass

    def test_get_subscription(self):
        """Test case for get_subscription

        Retrieve a subscription resource  # noqa: E501
        """
        pass

    def test_get_team(self):
        """Test case for get_team

        Retrieve a team resource  # noqa: E501
        """
        pass

    def test_update_team(self):
        """Test case for update_team

        Update a team  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
