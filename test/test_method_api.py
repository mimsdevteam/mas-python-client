# coding: utf-8

"""
    MAS

    Mims Api Server  # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.api.method_api import MethodApi  # noqa: E501
from mas.rest import ApiException


class TestMethodApi(unittest.TestCase):
    """MethodApi unit test stubs"""

    def setUp(self):
        self.api = mas.api.method_api.MethodApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete(self):
        """Test case for delete

        Delete a method   # noqa: E501
        """
        pass

    def test_get(self):
        """Test case for get

        Retrieve a method description  # noqa: E501
        """
        pass

    def test_list(self):
        """Test case for list

        Retrieve the list of methods  # noqa: E501
        """
        pass

    def test_post(self):
        """Test case for post

        Create a method  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
