# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.models.models_team import ModelsTeam  # noqa: E501
from mas.rest import ApiException


class TestModelsTeam(unittest.TestCase):
    """ModelsTeam unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testModelsTeam(self):
        """Test ModelsTeam"""
        # FIXME: construct object with mandatory attributes with example values
        # model = mas.models.models_team.ModelsTeam()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
