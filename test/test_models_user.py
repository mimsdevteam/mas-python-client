# coding: utf-8

"""
    MAS

    Mims Api Server  # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.models.models_user import ModelsUser  # noqa: E501
from mas.rest import ApiException


class TestModelsUser(unittest.TestCase):
    """ModelsUser unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testModelsUser(self):
        """Test ModelsUser"""
        # FIXME: construct object with mandatory attributes with example values
        # model = mas.models.models_user.ModelsUser()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
