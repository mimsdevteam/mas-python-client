# coding: utf-8

"""
    MAS

    Mims Api Server  # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import mas
from mas.models.wesmodels_exec_resources import WesmodelsExecResources  # noqa: E501
from mas.rest import ApiException


class TestWesmodelsExecResources(unittest.TestCase):
    """WesmodelsExecResources unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testWesmodelsExecResources(self):
        """Test WesmodelsExecResources"""
        # FIXME: construct object with mandatory attributes with example values
        # model = mas.models.wesmodels_exec_resources.WesmodelsExecResources()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
