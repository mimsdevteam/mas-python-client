# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsPostGroup(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'description': 'str',
        'metadata': 'dict(str, str)',
        'name': 'str'
    }

    attribute_map = {
        'description': 'description',
        'metadata': 'metadata',
        'name': 'name'
    }

    def __init__(self, description=None, metadata=None, name=None):  # noqa: E501
        """ModelsPostGroup - a model defined in Swagger"""  # noqa: E501

        self._description = None
        self._metadata = None
        self._name = None
        self.discriminator = None

        self.description = description
        self.metadata = metadata
        self.name = name

    @property
    def description(self):
        """Gets the description of this ModelsPostGroup.  # noqa: E501

        short description of the fileGroup  # noqa: E501

        :return: The description of this ModelsPostGroup.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ModelsPostGroup.

        short description of the fileGroup  # noqa: E501

        :param description: The description of this ModelsPostGroup.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def metadata(self):
        """Gets the metadata of this ModelsPostGroup.  # noqa: E501

        metadata associated with this resource  # noqa: E501

        :return: The metadata of this ModelsPostGroup.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this ModelsPostGroup.

        metadata associated with this resource  # noqa: E501

        :param metadata: The metadata of this ModelsPostGroup.  # noqa: E501
        :type: dict(str, str)
        """
        if metadata is None:
            raise ValueError("Invalid value for `metadata`, must not be `None`")  # noqa: E501

        self._metadata = metadata

    @property
    def name(self):
        """Gets the name of this ModelsPostGroup.  # noqa: E501

        name of the fileGroup  # noqa: E501

        :return: The name of this ModelsPostGroup.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ModelsPostGroup.

        name of the fileGroup  # noqa: E501

        :param name: The name of this ModelsPostGroup.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsPostGroup, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsPostGroup):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
