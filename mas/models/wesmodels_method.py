# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class WesmodelsMethod(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'contexts': 'dict(str, WesmodelsMethodContext)',
        'description': 'str',
        'docker_image': 'str',
        'name': 'str'
    }

    attribute_map = {
        'id': '_id',
        'contexts': 'contexts',
        'description': 'description',
        'docker_image': 'docker_image',
        'name': 'name'
    }

    def __init__(self, id=None, contexts=None, description=None, docker_image=None, name=None):  # noqa: E501
        """WesmodelsMethod - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._contexts = None
        self._description = None
        self._docker_image = None
        self._name = None
        self.discriminator = None

        self.id = id
        self.contexts = contexts
        self.description = description
        self.docker_image = docker_image
        self.name = name

    @property
    def id(self):
        """Gets the id of this WesmodelsMethod.  # noqa: E501


        :return: The id of this WesmodelsMethod.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this WesmodelsMethod.


        :param id: The id of this WesmodelsMethod.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def contexts(self):
        """Gets the contexts of this WesmodelsMethod.  # noqa: E501


        :return: The contexts of this WesmodelsMethod.  # noqa: E501
        :rtype: dict(str, WesmodelsMethodContext)
        """
        return self._contexts

    @contexts.setter
    def contexts(self, contexts):
        """Sets the contexts of this WesmodelsMethod.


        :param contexts: The contexts of this WesmodelsMethod.  # noqa: E501
        :type: dict(str, WesmodelsMethodContext)
        """
        if contexts is None:
            raise ValueError("Invalid value for `contexts`, must not be `None`")  # noqa: E501

        self._contexts = contexts

    @property
    def description(self):
        """Gets the description of this WesmodelsMethod.  # noqa: E501


        :return: The description of this WesmodelsMethod.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this WesmodelsMethod.


        :param description: The description of this WesmodelsMethod.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def docker_image(self):
        """Gets the docker_image of this WesmodelsMethod.  # noqa: E501


        :return: The docker_image of this WesmodelsMethod.  # noqa: E501
        :rtype: str
        """
        return self._docker_image

    @docker_image.setter
    def docker_image(self, docker_image):
        """Sets the docker_image of this WesmodelsMethod.


        :param docker_image: The docker_image of this WesmodelsMethod.  # noqa: E501
        :type: str
        """
        if docker_image is None:
            raise ValueError("Invalid value for `docker_image`, must not be `None`")  # noqa: E501

        self._docker_image = docker_image

    @property
    def name(self):
        """Gets the name of this WesmodelsMethod.  # noqa: E501


        :return: The name of this WesmodelsMethod.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this WesmodelsMethod.


        :param name: The name of this WesmodelsMethod.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(WesmodelsMethod, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, WesmodelsMethod):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
