# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsProjectCreateData(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'description': 'str',
        'metadata': 'dict(str, str)',
        'title': 'str'
    }

    attribute_map = {
        'description': 'description',
        'metadata': 'metadata',
        'title': 'title'
    }

    def __init__(self, description=None, metadata=None, title=None):  # noqa: E501
        """ModelsProjectCreateData - a model defined in Swagger"""  # noqa: E501

        self._description = None
        self._metadata = None
        self._title = None
        self.discriminator = None

        self.description = description
        self.metadata = metadata
        self.title = title

    @property
    def description(self):
        """Gets the description of this ModelsProjectCreateData.  # noqa: E501

        short description of the new project resource  # noqa: E501

        :return: The description of this ModelsProjectCreateData.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ModelsProjectCreateData.

        short description of the new project resource  # noqa: E501

        :param description: The description of this ModelsProjectCreateData.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def metadata(self):
        """Gets the metadata of this ModelsProjectCreateData.  # noqa: E501

        metadata to include at resource creation  # noqa: E501

        :return: The metadata of this ModelsProjectCreateData.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this ModelsProjectCreateData.

        metadata to include at resource creation  # noqa: E501

        :param metadata: The metadata of this ModelsProjectCreateData.  # noqa: E501
        :type: dict(str, str)
        """
        if metadata is None:
            raise ValueError("Invalid value for `metadata`, must not be `None`")  # noqa: E501

        self._metadata = metadata

    @property
    def title(self):
        """Gets the title of this ModelsProjectCreateData.  # noqa: E501

        title of the new project  # noqa: E501

        :return: The title of this ModelsProjectCreateData.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this ModelsProjectCreateData.

        title of the new project  # noqa: E501

        :param title: The title of this ModelsProjectCreateData.  # noqa: E501
        :type: str
        """
        if title is None:
            raise ValueError("Invalid value for `title`, must not be `None`")  # noqa: E501

        self._title = title

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsProjectCreateData, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsProjectCreateData):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
