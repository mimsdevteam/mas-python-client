# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class WesmodelsWorkflowInput(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'description': 'str',
        'metadata': 'dict(str, str)',
        'object_type': 'str',
        'optional': 'bool',
        'type': 'str'
    }

    attribute_map = {
        'description': 'description',
        'metadata': 'metadata',
        'object_type': 'object_type',
        'optional': 'optional',
        'type': 'type'
    }

    def __init__(self, description=None, metadata=None, object_type=None, optional=None, type=None):  # noqa: E501
        """WesmodelsWorkflowInput - a model defined in Swagger"""  # noqa: E501

        self._description = None
        self._metadata = None
        self._object_type = None
        self._optional = None
        self._type = None
        self.discriminator = None

        self.description = description
        self.metadata = metadata
        self.object_type = object_type
        self.optional = optional
        self.type = type

    @property
    def description(self):
        """Gets the description of this WesmodelsWorkflowInput.  # noqa: E501


        :return: The description of this WesmodelsWorkflowInput.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this WesmodelsWorkflowInput.


        :param description: The description of this WesmodelsWorkflowInput.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def metadata(self):
        """Gets the metadata of this WesmodelsWorkflowInput.  # noqa: E501


        :return: The metadata of this WesmodelsWorkflowInput.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this WesmodelsWorkflowInput.


        :param metadata: The metadata of this WesmodelsWorkflowInput.  # noqa: E501
        :type: dict(str, str)
        """
        if metadata is None:
            raise ValueError("Invalid value for `metadata`, must not be `None`")  # noqa: E501

        self._metadata = metadata

    @property
    def object_type(self):
        """Gets the object_type of this WesmodelsWorkflowInput.  # noqa: E501


        :return: The object_type of this WesmodelsWorkflowInput.  # noqa: E501
        :rtype: str
        """
        return self._object_type

    @object_type.setter
    def object_type(self, object_type):
        """Sets the object_type of this WesmodelsWorkflowInput.


        :param object_type: The object_type of this WesmodelsWorkflowInput.  # noqa: E501
        :type: str
        """
        if object_type is None:
            raise ValueError("Invalid value for `object_type`, must not be `None`")  # noqa: E501

        self._object_type = object_type

    @property
    def optional(self):
        """Gets the optional of this WesmodelsWorkflowInput.  # noqa: E501


        :return: The optional of this WesmodelsWorkflowInput.  # noqa: E501
        :rtype: bool
        """
        return self._optional

    @optional.setter
    def optional(self, optional):
        """Sets the optional of this WesmodelsWorkflowInput.


        :param optional: The optional of this WesmodelsWorkflowInput.  # noqa: E501
        :type: bool
        """
        if optional is None:
            raise ValueError("Invalid value for `optional`, must not be `None`")  # noqa: E501

        self._optional = optional

    @property
    def type(self):
        """Gets the type of this WesmodelsWorkflowInput.  # noqa: E501


        :return: The type of this WesmodelsWorkflowInput.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this WesmodelsWorkflowInput.


        :param type: The type of this WesmodelsWorkflowInput.  # noqa: E501
        :type: str
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(WesmodelsWorkflowInput, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, WesmodelsWorkflowInput):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
