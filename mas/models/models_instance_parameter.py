# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsInstanceParameter(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'input_description': 'str',
        'input_kind': 'str',
        'input_metadata': 'dict(str, str)',
        'input_name': 'str',
        'input_value': 'str'
    }

    attribute_map = {
        'input_description': 'input_description',
        'input_kind': 'input_kind',
        'input_metadata': 'input_metadata',
        'input_name': 'input_name',
        'input_value': 'input_value'
    }

    def __init__(self, input_description=None, input_kind=None, input_metadata=None, input_name=None, input_value=None):  # noqa: E501
        """ModelsInstanceParameter - a model defined in Swagger"""  # noqa: E501

        self._input_description = None
        self._input_kind = None
        self._input_metadata = None
        self._input_name = None
        self._input_value = None
        self.discriminator = None

        self.input_description = input_description
        self.input_kind = input_kind
        self.input_metadata = input_metadata
        self.input_name = input_name
        self.input_value = input_value

    @property
    def input_description(self):
        """Gets the input_description of this ModelsInstanceParameter.  # noqa: E501

        description of the input  # noqa: E501

        :return: The input_description of this ModelsInstanceParameter.  # noqa: E501
        :rtype: str
        """
        return self._input_description

    @input_description.setter
    def input_description(self, input_description):
        """Sets the input_description of this ModelsInstanceParameter.

        description of the input  # noqa: E501

        :param input_description: The input_description of this ModelsInstanceParameter.  # noqa: E501
        :type: str
        """
        if input_description is None:
            raise ValueError("Invalid value for `input_description`, must not be `None`")  # noqa: E501

        self._input_description = input_description

    @property
    def input_kind(self):
        """Gets the input_kind of this ModelsInstanceParameter.  # noqa: E501

        type of the input's target  # noqa: E501

        :return: The input_kind of this ModelsInstanceParameter.  # noqa: E501
        :rtype: str
        """
        return self._input_kind

    @input_kind.setter
    def input_kind(self, input_kind):
        """Sets the input_kind of this ModelsInstanceParameter.

        type of the input's target  # noqa: E501

        :param input_kind: The input_kind of this ModelsInstanceParameter.  # noqa: E501
        :type: str
        """
        if input_kind is None:
            raise ValueError("Invalid value for `input_kind`, must not be `None`")  # noqa: E501

        self._input_kind = input_kind

    @property
    def input_metadata(self):
        """Gets the input_metadata of this ModelsInstanceParameter.  # noqa: E501

        additional metadata  # noqa: E501

        :return: The input_metadata of this ModelsInstanceParameter.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._input_metadata

    @input_metadata.setter
    def input_metadata(self, input_metadata):
        """Sets the input_metadata of this ModelsInstanceParameter.

        additional metadata  # noqa: E501

        :param input_metadata: The input_metadata of this ModelsInstanceParameter.  # noqa: E501
        :type: dict(str, str)
        """
        if input_metadata is None:
            raise ValueError("Invalid value for `input_metadata`, must not be `None`")  # noqa: E501

        self._input_metadata = input_metadata

    @property
    def input_name(self):
        """Gets the input_name of this ModelsInstanceParameter.  # noqa: E501

        name of the input  # noqa: E501

        :return: The input_name of this ModelsInstanceParameter.  # noqa: E501
        :rtype: str
        """
        return self._input_name

    @input_name.setter
    def input_name(self, input_name):
        """Sets the input_name of this ModelsInstanceParameter.

        name of the input  # noqa: E501

        :param input_name: The input_name of this ModelsInstanceParameter.  # noqa: E501
        :type: str
        """
        if input_name is None:
            raise ValueError("Invalid value for `input_name`, must not be `None`")  # noqa: E501

        self._input_name = input_name

    @property
    def input_value(self):
        """Gets the input_value of this ModelsInstanceParameter.  # noqa: E501

        identifier of the input's target  # noqa: E501

        :return: The input_value of this ModelsInstanceParameter.  # noqa: E501
        :rtype: str
        """
        return self._input_value

    @input_value.setter
    def input_value(self, input_value):
        """Sets the input_value of this ModelsInstanceParameter.

        identifier of the input's target  # noqa: E501

        :param input_value: The input_value of this ModelsInstanceParameter.  # noqa: E501
        :type: str
        """
        if input_value is None:
            raise ValueError("Invalid value for `input_value`, must not be `None`")  # noqa: E501

        self._input_value = input_value

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsInstanceParameter, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsInstanceParameter):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
