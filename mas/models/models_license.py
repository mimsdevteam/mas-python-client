# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsLicense(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'key': 'str',
        'description': 'str',
        'name': 'str',
        'permissions': 'dict(str, ModelsLicensePermissions)'
    }

    attribute_map = {
        'key': '_key',
        'description': 'description',
        'name': 'name',
        'permissions': 'permissions'
    }

    def __init__(self, key=None, description=None, name=None, permissions=None):  # noqa: E501
        """ModelsLicense - a model defined in Swagger"""  # noqa: E501

        self._key = None
        self._description = None
        self._name = None
        self._permissions = None
        self.discriminator = None

        if key is not None:
            self.key = key
        self.description = description
        self.name = name
        self.permissions = permissions

    @property
    def key(self):
        """Gets the key of this ModelsLicense.  # noqa: E501


        :return: The key of this ModelsLicense.  # noqa: E501
        :rtype: str
        """
        return self._key

    @key.setter
    def key(self, key):
        """Sets the key of this ModelsLicense.


        :param key: The key of this ModelsLicense.  # noqa: E501
        :type: str
        """

        self._key = key

    @property
    def description(self):
        """Gets the description of this ModelsLicense.  # noqa: E501


        :return: The description of this ModelsLicense.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ModelsLicense.


        :param description: The description of this ModelsLicense.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def name(self):
        """Gets the name of this ModelsLicense.  # noqa: E501


        :return: The name of this ModelsLicense.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ModelsLicense.


        :param name: The name of this ModelsLicense.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def permissions(self):
        """Gets the permissions of this ModelsLicense.  # noqa: E501


        :return: The permissions of this ModelsLicense.  # noqa: E501
        :rtype: dict(str, ModelsLicensePermissions)
        """
        return self._permissions

    @permissions.setter
    def permissions(self, permissions):
        """Sets the permissions of this ModelsLicense.


        :param permissions: The permissions of this ModelsLicense.  # noqa: E501
        :type: dict(str, ModelsLicensePermissions)
        """
        if permissions is None:
            raise ValueError("Invalid value for `permissions`, must not be `None`")  # noqa: E501

        self._permissions = permissions

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsLicense, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsLicense):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
