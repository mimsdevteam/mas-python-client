# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsInternalizeAccessResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'internal_access_token': 'str',
        'internal_refresh_token': 'str'
    }

    attribute_map = {
        'internal_access_token': 'internal_access_token',
        'internal_refresh_token': 'internal_refresh_token'
    }

    def __init__(self, internal_access_token=None, internal_refresh_token=None):  # noqa: E501
        """ModelsInternalizeAccessResponse - a model defined in Swagger"""  # noqa: E501

        self._internal_access_token = None
        self._internal_refresh_token = None
        self.discriminator = None

        self.internal_access_token = internal_access_token
        self.internal_refresh_token = internal_refresh_token

    @property
    def internal_access_token(self):
        """Gets the internal_access_token of this ModelsInternalizeAccessResponse.  # noqa: E501


        :return: The internal_access_token of this ModelsInternalizeAccessResponse.  # noqa: E501
        :rtype: str
        """
        return self._internal_access_token

    @internal_access_token.setter
    def internal_access_token(self, internal_access_token):
        """Sets the internal_access_token of this ModelsInternalizeAccessResponse.


        :param internal_access_token: The internal_access_token of this ModelsInternalizeAccessResponse.  # noqa: E501
        :type: str
        """
        if internal_access_token is None:
            raise ValueError("Invalid value for `internal_access_token`, must not be `None`")  # noqa: E501

        self._internal_access_token = internal_access_token

    @property
    def internal_refresh_token(self):
        """Gets the internal_refresh_token of this ModelsInternalizeAccessResponse.  # noqa: E501


        :return: The internal_refresh_token of this ModelsInternalizeAccessResponse.  # noqa: E501
        :rtype: str
        """
        return self._internal_refresh_token

    @internal_refresh_token.setter
    def internal_refresh_token(self, internal_refresh_token):
        """Sets the internal_refresh_token of this ModelsInternalizeAccessResponse.


        :param internal_refresh_token: The internal_refresh_token of this ModelsInternalizeAccessResponse.  # noqa: E501
        :type: str
        """
        if internal_refresh_token is None:
            raise ValueError("Invalid value for `internal_refresh_token`, must not be `None`")  # noqa: E501

        self._internal_refresh_token = internal_refresh_token

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsInternalizeAccessResponse, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsInternalizeAccessResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
