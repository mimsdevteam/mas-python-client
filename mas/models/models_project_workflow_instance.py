# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsProjectWorkflowInstance(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_by': 'ModelsEntity',
        'dashboard_data_id': 'str',
        'dashboard_files': 'dict(str, str)',
        'date_of_creation': 'int',
        'end_time': 'int',
        'id': 'str',
        'parameters': 'list[ModelsInstanceParameter]',
        'start_time': 'int',
        'status': 'str',
        'workflow_id': 'str'
    }

    attribute_map = {
        'created_by': 'created_by',
        'dashboard_data_id': 'dashboard_data_id',
        'dashboard_files': 'dashboard_files',
        'date_of_creation': 'date_of_creation',
        'end_time': 'end_time',
        'id': 'id',
        'parameters': 'parameters',
        'start_time': 'start_time',
        'status': 'status',
        'workflow_id': 'workflow_id'
    }

    def __init__(self, created_by=None, dashboard_data_id=None, dashboard_files=None, date_of_creation=None, end_time=None, id=None, parameters=None, start_time=None, status=None, workflow_id=None):  # noqa: E501
        """ModelsProjectWorkflowInstance - a model defined in Swagger"""  # noqa: E501

        self._created_by = None
        self._dashboard_data_id = None
        self._dashboard_files = None
        self._date_of_creation = None
        self._end_time = None
        self._id = None
        self._parameters = None
        self._start_time = None
        self._status = None
        self._workflow_id = None
        self.discriminator = None

        self.created_by = created_by
        self.dashboard_data_id = dashboard_data_id
        self.dashboard_files = dashboard_files
        self.date_of_creation = date_of_creation
        self.end_time = end_time
        self.id = id
        self.parameters = parameters
        self.start_time = start_time
        self.status = status
        self.workflow_id = workflow_id

    @property
    def created_by(self):
        """Gets the created_by of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The created_by of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: ModelsEntity
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this ModelsProjectWorkflowInstance.


        :param created_by: The created_by of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: ModelsEntity
        """
        if created_by is None:
            raise ValueError("Invalid value for `created_by`, must not be `None`")  # noqa: E501

        self._created_by = created_by

    @property
    def dashboard_data_id(self):
        """Gets the dashboard_data_id of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The dashboard_data_id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: str
        """
        return self._dashboard_data_id

    @dashboard_data_id.setter
    def dashboard_data_id(self, dashboard_data_id):
        """Sets the dashboard_data_id of this ModelsProjectWorkflowInstance.


        :param dashboard_data_id: The dashboard_data_id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: str
        """
        if dashboard_data_id is None:
            raise ValueError("Invalid value for `dashboard_data_id`, must not be `None`")  # noqa: E501

        self._dashboard_data_id = dashboard_data_id

    @property
    def dashboard_files(self):
        """Gets the dashboard_files of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The dashboard_files of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._dashboard_files

    @dashboard_files.setter
    def dashboard_files(self, dashboard_files):
        """Sets the dashboard_files of this ModelsProjectWorkflowInstance.


        :param dashboard_files: The dashboard_files of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: dict(str, str)
        """
        if dashboard_files is None:
            raise ValueError("Invalid value for `dashboard_files`, must not be `None`")  # noqa: E501

        self._dashboard_files = dashboard_files

    @property
    def date_of_creation(self):
        """Gets the date_of_creation of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The date_of_creation of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: int
        """
        return self._date_of_creation

    @date_of_creation.setter
    def date_of_creation(self, date_of_creation):
        """Sets the date_of_creation of this ModelsProjectWorkflowInstance.


        :param date_of_creation: The date_of_creation of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: int
        """
        if date_of_creation is None:
            raise ValueError("Invalid value for `date_of_creation`, must not be `None`")  # noqa: E501

        self._date_of_creation = date_of_creation

    @property
    def end_time(self):
        """Gets the end_time of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The end_time of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: int
        """
        return self._end_time

    @end_time.setter
    def end_time(self, end_time):
        """Sets the end_time of this ModelsProjectWorkflowInstance.


        :param end_time: The end_time of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: int
        """
        if end_time is None:
            raise ValueError("Invalid value for `end_time`, must not be `None`")  # noqa: E501

        self._end_time = end_time

    @property
    def id(self):
        """Gets the id of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ModelsProjectWorkflowInstance.


        :param id: The id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def parameters(self):
        """Gets the parameters of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The parameters of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: list[ModelsInstanceParameter]
        """
        return self._parameters

    @parameters.setter
    def parameters(self, parameters):
        """Sets the parameters of this ModelsProjectWorkflowInstance.


        :param parameters: The parameters of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: list[ModelsInstanceParameter]
        """
        if parameters is None:
            raise ValueError("Invalid value for `parameters`, must not be `None`")  # noqa: E501

        self._parameters = parameters

    @property
    def start_time(self):
        """Gets the start_time of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The start_time of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: int
        """
        return self._start_time

    @start_time.setter
    def start_time(self, start_time):
        """Sets the start_time of this ModelsProjectWorkflowInstance.


        :param start_time: The start_time of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: int
        """
        if start_time is None:
            raise ValueError("Invalid value for `start_time`, must not be `None`")  # noqa: E501

        self._start_time = start_time

    @property
    def status(self):
        """Gets the status of this ModelsProjectWorkflowInstance.  # noqa: E501


        :return: The status of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this ModelsProjectWorkflowInstance.


        :param status: The status of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        self._status = status

    @property
    def workflow_id(self):
        """Gets the workflow_id of this ModelsProjectWorkflowInstance.  # noqa: E501

        WorkflowID that was used  # noqa: E501

        :return: The workflow_id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :rtype: str
        """
        return self._workflow_id

    @workflow_id.setter
    def workflow_id(self, workflow_id):
        """Sets the workflow_id of this ModelsProjectWorkflowInstance.

        WorkflowID that was used  # noqa: E501

        :param workflow_id: The workflow_id of this ModelsProjectWorkflowInstance.  # noqa: E501
        :type: str
        """
        if workflow_id is None:
            raise ValueError("Invalid value for `workflow_id`, must not be `None`")  # noqa: E501

        self._workflow_id = workflow_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsProjectWorkflowInstance, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsProjectWorkflowInstance):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
