# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsUserGroup(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'capabilities': 'ModelsUserGroupCapabilities',
        'date_of_creation': 'int',
        'date_of_modification': 'int',
        'description': 'str',
        'id': 'str',
        'kind': 'str',
        'last_modified_by': 'ModelsEntity',
        'links': 'object',
        'metadata': 'dict(str, str)',
        'name': 'str',
        'owned_by_me': 'bool',
        'owner': 'ModelsEntity',
        'permissions': 'list[ModelsPermission]'
    }

    attribute_map = {
        'capabilities': 'capabilities',
        'date_of_creation': 'date_of_creation',
        'date_of_modification': 'date_of_modification',
        'description': 'description',
        'id': 'id',
        'kind': 'kind',
        'last_modified_by': 'last_modified_by',
        'links': 'links',
        'metadata': 'metadata',
        'name': 'name',
        'owned_by_me': 'owned_by_me',
        'owner': 'owner',
        'permissions': 'permissions'
    }

    def __init__(self, capabilities=None, date_of_creation=None, date_of_modification=None, description=None, id=None, kind=None, last_modified_by=None, links=None, metadata=None, name=None, owned_by_me=None, owner=None, permissions=None):  # noqa: E501
        """ModelsUserGroup - a model defined in Swagger"""  # noqa: E501

        self._capabilities = None
        self._date_of_creation = None
        self._date_of_modification = None
        self._description = None
        self._id = None
        self._kind = None
        self._last_modified_by = None
        self._links = None
        self._metadata = None
        self._name = None
        self._owned_by_me = None
        self._owner = None
        self._permissions = None
        self.discriminator = None

        if capabilities is not None:
            self.capabilities = capabilities
        self.date_of_creation = date_of_creation
        self.date_of_modification = date_of_modification
        self.description = description
        self.id = id
        self.kind = kind
        self.last_modified_by = last_modified_by
        self.links = links
        self.metadata = metadata
        if name is not None:
            self.name = name
        self.owned_by_me = owned_by_me
        self.owner = owner
        self.permissions = permissions

    @property
    def capabilities(self):
        """Gets the capabilities of this ModelsUserGroup.  # noqa: E501

        capabilities on this userGroup, for the current user  # noqa: E501

        :return: The capabilities of this ModelsUserGroup.  # noqa: E501
        :rtype: ModelsUserGroupCapabilities
        """
        return self._capabilities

    @capabilities.setter
    def capabilities(self, capabilities):
        """Sets the capabilities of this ModelsUserGroup.

        capabilities on this userGroup, for the current user  # noqa: E501

        :param capabilities: The capabilities of this ModelsUserGroup.  # noqa: E501
        :type: ModelsUserGroupCapabilities
        """

        self._capabilities = capabilities

    @property
    def date_of_creation(self):
        """Gets the date_of_creation of this ModelsUserGroup.  # noqa: E501

        UNIX timestamp of the creation date of the userGroup  # noqa: E501

        :return: The date_of_creation of this ModelsUserGroup.  # noqa: E501
        :rtype: int
        """
        return self._date_of_creation

    @date_of_creation.setter
    def date_of_creation(self, date_of_creation):
        """Sets the date_of_creation of this ModelsUserGroup.

        UNIX timestamp of the creation date of the userGroup  # noqa: E501

        :param date_of_creation: The date_of_creation of this ModelsUserGroup.  # noqa: E501
        :type: int
        """
        if date_of_creation is None:
            raise ValueError("Invalid value for `date_of_creation`, must not be `None`")  # noqa: E501

        self._date_of_creation = date_of_creation

    @property
    def date_of_modification(self):
        """Gets the date_of_modification of this ModelsUserGroup.  # noqa: E501

        UNIX timestamp of the last modification date on the userGroup  # noqa: E501

        :return: The date_of_modification of this ModelsUserGroup.  # noqa: E501
        :rtype: int
        """
        return self._date_of_modification

    @date_of_modification.setter
    def date_of_modification(self, date_of_modification):
        """Sets the date_of_modification of this ModelsUserGroup.

        UNIX timestamp of the last modification date on the userGroup  # noqa: E501

        :param date_of_modification: The date_of_modification of this ModelsUserGroup.  # noqa: E501
        :type: int
        """
        if date_of_modification is None:
            raise ValueError("Invalid value for `date_of_modification`, must not be `None`")  # noqa: E501

        self._date_of_modification = date_of_modification

    @property
    def description(self):
        """Gets the description of this ModelsUserGroup.  # noqa: E501

        short description the userGroup  # noqa: E501

        :return: The description of this ModelsUserGroup.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ModelsUserGroup.

        short description the userGroup  # noqa: E501

        :param description: The description of this ModelsUserGroup.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def id(self):
        """Gets the id of this ModelsUserGroup.  # noqa: E501

        unique identifier for this userGroup resource  # noqa: E501

        :return: The id of this ModelsUserGroup.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ModelsUserGroup.

        unique identifier for this userGroup resource  # noqa: E501

        :param id: The id of this ModelsUserGroup.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def kind(self):
        """Gets the kind of this ModelsUserGroup.  # noqa: E501

        kind of this resource  # noqa: E501

        :return: The kind of this ModelsUserGroup.  # noqa: E501
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind):
        """Sets the kind of this ModelsUserGroup.

        kind of this resource  # noqa: E501

        :param kind: The kind of this ModelsUserGroup.  # noqa: E501
        :type: str
        """
        if kind is None:
            raise ValueError("Invalid value for `kind`, must not be `None`")  # noqa: E501
        allowed_values = ["usergroups"]  # noqa: E501
        if kind not in allowed_values:
            raise ValueError(
                "Invalid value for `kind` ({0}), must be one of {1}"  # noqa: E501
                .format(kind, allowed_values)
            )

        self._kind = kind

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this ModelsUserGroup.  # noqa: E501

        user who modified the userGroup last as a user resource  # noqa: E501

        :return: The last_modified_by of this ModelsUserGroup.  # noqa: E501
        :rtype: ModelsEntity
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this ModelsUserGroup.

        user who modified the userGroup last as a user resource  # noqa: E501

        :param last_modified_by: The last_modified_by of this ModelsUserGroup.  # noqa: E501
        :type: ModelsEntity
        """
        if last_modified_by is None:
            raise ValueError("Invalid value for `last_modified_by`, must not be `None`")  # noqa: E501

        self._last_modified_by = last_modified_by

    @property
    def links(self):
        """Gets the links of this ModelsUserGroup.  # noqa: E501

        contains a \"self\" and \"download\" link targeting this userGroup resource  # noqa: E501

        :return: The links of this ModelsUserGroup.  # noqa: E501
        :rtype: object
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this ModelsUserGroup.

        contains a \"self\" and \"download\" link targeting this userGroup resource  # noqa: E501

        :param links: The links of this ModelsUserGroup.  # noqa: E501
        :type: object
        """
        if links is None:
            raise ValueError("Invalid value for `links`, must not be `None`")  # noqa: E501

        self._links = links

    @property
    def metadata(self):
        """Gets the metadata of this ModelsUserGroup.  # noqa: E501

        metadata of the userGroup  # noqa: E501

        :return: The metadata of this ModelsUserGroup.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this ModelsUserGroup.

        metadata of the userGroup  # noqa: E501

        :param metadata: The metadata of this ModelsUserGroup.  # noqa: E501
        :type: dict(str, str)
        """
        if metadata is None:
            raise ValueError("Invalid value for `metadata`, must not be `None`")  # noqa: E501

        self._metadata = metadata

    @property
    def name(self):
        """Gets the name of this ModelsUserGroup.  # noqa: E501

        display name of the user  # noqa: E501

        :return: The name of this ModelsUserGroup.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ModelsUserGroup.

        display name of the user  # noqa: E501

        :param name: The name of this ModelsUserGroup.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def owned_by_me(self):
        """Gets the owned_by_me of this ModelsUserGroup.  # noqa: E501

        true if the user is the owner of the userGroup  # noqa: E501

        :return: The owned_by_me of this ModelsUserGroup.  # noqa: E501
        :rtype: bool
        """
        return self._owned_by_me

    @owned_by_me.setter
    def owned_by_me(self, owned_by_me):
        """Sets the owned_by_me of this ModelsUserGroup.

        true if the user is the owner of the userGroup  # noqa: E501

        :param owned_by_me: The owned_by_me of this ModelsUserGroup.  # noqa: E501
        :type: bool
        """
        if owned_by_me is None:
            raise ValueError("Invalid value for `owned_by_me`, must not be `None`")  # noqa: E501

        self._owned_by_me = owned_by_me

    @property
    def owner(self):
        """Gets the owner of this ModelsUserGroup.  # noqa: E501

        owner of the userGroup as a user resource  # noqa: E501

        :return: The owner of this ModelsUserGroup.  # noqa: E501
        :rtype: ModelsEntity
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this ModelsUserGroup.

        owner of the userGroup as a user resource  # noqa: E501

        :param owner: The owner of this ModelsUserGroup.  # noqa: E501
        :type: ModelsEntity
        """
        if owner is None:
            raise ValueError("Invalid value for `owner`, must not be `None`")  # noqa: E501

        self._owner = owner

    @property
    def permissions(self):
        """Gets the permissions of this ModelsUserGroup.  # noqa: E501

        list of permissions resources associated with this userGroup  # noqa: E501

        :return: The permissions of this ModelsUserGroup.  # noqa: E501
        :rtype: list[ModelsPermission]
        """
        return self._permissions

    @permissions.setter
    def permissions(self, permissions):
        """Sets the permissions of this ModelsUserGroup.

        list of permissions resources associated with this userGroup  # noqa: E501

        :param permissions: The permissions of this ModelsUserGroup.  # noqa: E501
        :type: list[ModelsPermission]
        """
        if permissions is None:
            raise ValueError("Invalid value for `permissions`, must not be `None`")  # noqa: E501

        self._permissions = permissions

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsUserGroup, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsUserGroup):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
