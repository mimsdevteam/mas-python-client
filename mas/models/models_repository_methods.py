# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsRepositoryMethods(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'count': 'int',
        'method_list': 'list[str]'
    }

    attribute_map = {
        'count': 'count',
        'method_list': 'method_list'
    }

    def __init__(self, count=None, method_list=None):  # noqa: E501
        """ModelsRepositoryMethods - a model defined in Swagger"""  # noqa: E501

        self._count = None
        self._method_list = None
        self.discriminator = None

        self.count = count
        self.method_list = method_list

    @property
    def count(self):
        """Gets the count of this ModelsRepositoryMethods.  # noqa: E501

        number of elements returned by the request  # noqa: E501

        :return: The count of this ModelsRepositoryMethods.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this ModelsRepositoryMethods.

        number of elements returned by the request  # noqa: E501

        :param count: The count of this ModelsRepositoryMethods.  # noqa: E501
        :type: int
        """
        if count is None:
            raise ValueError("Invalid value for `count`, must not be `None`")  # noqa: E501

        self._count = count

    @property
    def method_list(self):
        """Gets the method_list of this ModelsRepositoryMethods.  # noqa: E501


        :return: The method_list of this ModelsRepositoryMethods.  # noqa: E501
        :rtype: list[str]
        """
        return self._method_list

    @method_list.setter
    def method_list(self, method_list):
        """Sets the method_list of this ModelsRepositoryMethods.


        :param method_list: The method_list of this ModelsRepositoryMethods.  # noqa: E501
        :type: list[str]
        """
        if method_list is None:
            raise ValueError("Invalid value for `method_list`, must not be `None`")  # noqa: E501

        self._method_list = method_list

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsRepositoryMethods, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsRepositoryMethods):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
