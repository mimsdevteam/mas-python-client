# coding: utf-8

"""
    MAS

    MIMS API SERVER      Roles (ASC):      1 Reader      2 Downloader      3 Writer      4 Contributor      5 Owner       # noqa: E501

    OpenAPI spec version: 1.7.3
    Contact: tony@mims.ai
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ModelsFileGroupInfo(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'kind': 'str',
        'name': 'str'
    }

    attribute_map = {
        'id': 'id',
        'kind': 'kind',
        'name': 'name'
    }

    def __init__(self, id=None, kind=None, name=None):  # noqa: E501
        """ModelsFileGroupInfo - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._kind = None
        self._name = None
        self.discriminator = None

        self.id = id
        self.kind = kind
        self.name = name

    @property
    def id(self):
        """Gets the id of this ModelsFileGroupInfo.  # noqa: E501

        unique identifier for this filegroup resource  # noqa: E501

        :return: The id of this ModelsFileGroupInfo.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ModelsFileGroupInfo.

        unique identifier for this filegroup resource  # noqa: E501

        :param id: The id of this ModelsFileGroupInfo.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def kind(self):
        """Gets the kind of this ModelsFileGroupInfo.  # noqa: E501

        kind of this resource - \"filegroups\"  # noqa: E501

        :return: The kind of this ModelsFileGroupInfo.  # noqa: E501
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind):
        """Sets the kind of this ModelsFileGroupInfo.

        kind of this resource - \"filegroups\"  # noqa: E501

        :param kind: The kind of this ModelsFileGroupInfo.  # noqa: E501
        :type: str
        """
        if kind is None:
            raise ValueError("Invalid value for `kind`, must not be `None`")  # noqa: E501

        self._kind = kind

    @property
    def name(self):
        """Gets the name of this ModelsFileGroupInfo.  # noqa: E501

        display name of the filegroup  # noqa: E501

        :return: The name of this ModelsFileGroupInfo.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ModelsFileGroupInfo.

        display name of the filegroup  # noqa: E501

        :param name: The name of this ModelsFileGroupInfo.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ModelsFileGroupInfo, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ModelsFileGroupInfo):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
